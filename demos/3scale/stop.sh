#!/bin/bash
NS=demo-3scale

echo -e "\e[1m\e[34mINFO\e[0m  \e[1m\e[96mBegin\e[0m \e[1m3scale\e[0m demo \e[1mshutdown\e[0m"
oc scale --replicas=0 deployment.apps/manpage -n $NS
oc get all -n $NS
echo -e "\e[1m\e[34mINFO\e[0m  \e[1m\e[96mEnd\e[0m \e[1m3scale\e[0m demo \e[1mshutdown\e[0m"

