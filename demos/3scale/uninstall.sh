#!/bin/bash
SVC=3scale
NS=demo-$SVC

echo -e "\e[1m\e[34mINFO\e[0m  \e[1m\e[96mBegin\e[0m \e[1m$SVC\e[0m demo \e[1muninstallation\e[0m"
sxcm demo disable $SVC 
oc get all -n $NS
echo -e "\e[1m\e[34mINFO\e[0m  \e[1m\e[96mEnd\e[0m \e[1m$SVC\e[0m demo \e[1muninstallation\e[0m"
