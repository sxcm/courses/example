#!/bin/bash
SVC=3scale
NS=demo-$SVC

echo -e "\e[1m\e[34mINFO\e[0m  \e[1m\e[96mBegin\e[0m \e[1m$SVC\e[0m demo \e[1minstallation\e[0m"
sxcm resource enable $SVC
sxcm demo enable $SVC 
oc get all -n $NS
echo -e "\e[1m\e[34mINFO\e[0m  \e[1m\e[96mEnd\e[0m \e[1m$SVC\e[0m demo \e[1minstallation\e[0m"
