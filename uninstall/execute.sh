#!/bin/bash

echo "UNINSTALL === Disable cluster resource"
sxcm resource disable pipeline
sxcm resource disable istio
sxcm resource disable couchbase
sxcm resource disable sso
sxcm resource disable knative
sxcm resource disable 3scale
sxcm resource disable quay
sxcm resource disable vault
sxcm resource disable workspaces
sxcm resource disable acm
sxcm resource disable acs
sxcm resource disable kubevirt
sxcm resource disable gitlab
sxcm resource disable automation

echo "UNINSTALL === Disable cluster demo"
sxcm demo disable 3scale 
sxcm demo disable ansible
sxcm demo disable argocd
sxcm demo disable couchbase
sxcm demo disable istio
sxcm demo disable jenkins
sxcm demo disable knative
sxcm demo disable kubevirt
sxcm demo disable ocs
sxcm demo disable storage
sxcm demo disable vault
sxcm demo disable workspace

echo "UNINSTALL === Information"
sxcm info

